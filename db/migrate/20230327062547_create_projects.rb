class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.bigint :user_id
      t.string :version

      t.timestamps
    end
  end
end
