class UsersController < ApplicationController
  before_action :authenticate_request!, except: %i[login create]
  before_action :set_email, only: [:login]

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      UserNotifierMailer.send_signup_email(@user).deliver
      render json: { user: @user }, status: :ok
    else
      render json: { error: @user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  # POST /login
  def login
    @users = User.find_by(email: user_params[:email])
    exp = Time.now.to_i + 3600
    payload = { user_id: @users.id, exp: }
    # refres = { user_id: @users.id, exp: exp + (24 * 3600) }

    userss = @users.authenticate_password(user_params[:password])
    if @users && userss
      token = encode_token(payload)
      render json: { user: @users.new_attributes, token: }, status: :ok
    else
      render json: { error: 'Invalid username or password' },
             status: :unprocessable_entity
    end
  end

  private

  def user_params
    params.permit(:name, :email, :password)
  end

  def set_email
    @user = User.find_by(email: user_params[:email])
    return unless @user.nil?

    render json: { error: 'user atau password salah!!' }, status: :not_found
  end
end
