class BooksController < ApplicationController
  before_action :authenticate_request!, except: :login
  before_action :authorization!, only: %i[index show update destroy]

  def index
    render json: Book.all, status: :ok
  end

  def search
    q = book_params['s']
    book = Book.search_data("%#{q}%")
    render json: book
  end

  private

  def book_params
    params.permit(:s)
  end
end
