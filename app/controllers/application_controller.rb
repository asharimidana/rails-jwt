class ApplicationController < ActionController::API
  def encode_token(payload)
    JWT.encode payload, ENV['SECRET_KEY'], 'HS256'
  end

  attr_reader :current_user

  protected

  def authorization!
    # pengecekan dari role terkecil
    ability = Ability.new(@current_user)
    return if ability.can? :read, Book
    (ability.can? :manage, :all) ? return : (render json: 'Forbidden!!', status: :forbidden)
  end

  def authenticate_request!
    unless user_id_in_token?
      render json: { errors: ['Not Authenticated'] }, status: :unauthorized
      return
    end
    @current_user = User.find(auth_token[0]['user_id'])
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Forbidden'] }, status: :forbidden
  end

  private

  def http_token
    @http_token ||=
      (request.headers['Authorization'].split(' ').last if request.headers['Authorization'].present?)
  end

  def auth_token
    @auth_token ||=
      JWT.decode(http_token, ENV['SECRET_KEY'], true, { algorithm: 'HS256' })
  end

  def user_id_in_token?
    http_token && auth_token
  end
end
